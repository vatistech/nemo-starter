# nemo-starter

Wrapper over NeMo original repository that helps us managing changes on the original code.
https://github.com/NVIDIA/NeMo

The NeMo repository is added as a remote repo under the `nemo` name.

### Procedures

1. To test a new NeMo version:
    * create a new branch from nemo-start's `master` branch (convention: `version/<nemo_release_version>`)
    * in the new branch, merge the NeMo branch with the version you want to test. If any conflict not caused by you occurs, just `Accept Theirs`
    * merge the new branch in the `master` branch if and ONLY IF you want to stay with the new version. <br>
    **The recommended** way is to keep the new version in a separate branch for a while to prove it's stability. After the migration, the previous 
    version should be kept on a separate branch as a back-up.

2. Any addition to the original code shall be placed in the `nemo.vatis` package. <br>
    Changes to the original NeMo code should be avoided as much as possible.

3. The tags created on the current branch should be based on a NeMo tag and respect the following convention:
    * if the current nemo-starter tag is just a clone of a NeMo tag, then the nemo-starter tag should be named as `vatis-<nemo_tag>` (e.g. `vatis-r1.0.0rc1`)
    * if there are developments done based on a NeMo tag, the nemo-starter tag should be named as `vatis_<custom_tag>-<nemo_tag>`  
    For example:
        * `vatis_v1-r1.0.0rc1`
        * `vatis_v2-r1.0.0rc1`
        In this example, `v1` and `v2` are tags existent only under the bigger tag `r1.0.0rc1` from the NeMo repo.
        When the biggest tag is changed, the inner tags restart from the beginning. For example:
            * `vatis_v1-v1.0.1`
            * `vatis_v2-v1.0.1`  
    
4. To add new dependencies:
    1. go to `requirements` folder
    2. add the dependency in the specific `requirements` file (e.g. in `requirements_asr.txt` for an asr dependency, or in `requirements_general.txt` for a general purpose dependency)
    3. update the `requirements.txt` with the new dependency placed in the right batch. (the file is formed by concatenating the contents from `requirements_general.txt`, `requirements_asr.txt` and so on, so you'll have to find the batch in witch the dependency should be placed)
